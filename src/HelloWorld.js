import React from 'react';

function HelloWorld() {
  React.useEffect(() => {
    console.log('HelloWorld mounted');
    return () => {
      console.log('HelloWorld unmounted');
    }
  }, []);

  return <h1>Yeah! Counter is 10!</h1>;
}

export default HelloWorld;
