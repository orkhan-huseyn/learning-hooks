import React from 'react';
import { useCounter } from './useCounter';

function App() {
  const [counter, increment] = useCounter();

  return (
    <div>
      <p>You clicked {counter} times</p>
      <button onClick={() => increment()}>
        Click me
      </button>
    </div>
  );
}

// class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       counter: 0,
//     };
//     this.handleIncrement = this.handleIncrement.bind(this);
//   }
//
//   componentDidUpdate(prevProps, prevState) {
//     const { counter } = this.state;
//     if (prevState.counter !== counter) {
//       document.title = `Counter ${counter}`;
//     }
//   }
//
//   handleIncrement() {
//     const { counter } = this.state;
//     this.setState({
//       counter: counter + 1,
//     });
//   }
//
//   render() {
//     const { counter } = this.state;
//     return (
//       <div>
//         <p>You clicked {counter} times</p>
//         <button onClick={this.handleIncrement}>
//           Click me
//         </button>
//       </div>
//     );
//   }
// }

export default App;
