import React from 'react';

export function useCounter() {
  const [counter, setCounter] = React.useState(0);

  React.useEffect(() => {
    document.title = `Counter ${counter}`;
  }, [counter]);

  function increment(step = 1) {
    setCounter(counter + step);
  }

  return [counter, increment];
}
